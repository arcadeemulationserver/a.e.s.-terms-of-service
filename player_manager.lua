local S = minetest.get_translator("terms_of_service")

local function make_formspec() end

local TOS = [[
]] .. "1) " .. S("Always respect other players") .. "\n" .. [[
]] .. "2) " .. S("No excessive swearing") .. "\n" .. [[
]] .. "3) " .. S("No modified clients") .. "\n" .. [[
]] .. "\n" .. [[
]] .. S("Admins reserve the right to remove whoever transgress the rules. In case of repeated misbehaviours, the user will be banned")



minetest.register_on_joinplayer(function(player)

	local p_name = player:get_player_name()

	if not minetest.get_player_privs(p_name).tos_accepted then
		minetest.after(1, function()
			minetest.show_formspec(p_name, "tos:tos_fs", make_formspec())
		end)
	end
end)



minetest.register_on_player_receive_fields(function(player, formname, fields)

	if formname ~= "tos:tos_fs" then return end

	local p_name = player:get_player_name()

	if fields.ok or fields.key_enter then

		if fields.hidden_word == string.lower(fields.entry) then
			local privs = minetest.get_player_privs(p_name)
			privs.tos_accepted = true
			privs.interact = true
			minetest.set_player_privs(p_name, privs)
			minetest.chat_send_player(p_name, S("Enjoy your staying!"))
			
			if minetest.get_modpath("aes_xp") then
				--register some achievements
				aes_xp.add_achievement("Lawyer", 5, 0, 0, "Show you understand the rules", "tos.png")
				aes_xp.achieve(p_name,"Lawyer")
			end

		else
			minetest.chat_send_player(p_name, S("Wrong, try again"))
			minetest.show_formspec(p_name, "tos:tos_fs", make_formspec())
			return
		end

	elseif fields.quit then
			if not minetest.get_player_privs(p_name).tos_accepted then
				minetest.chat_send_player(p_name, S("Please show you understand the rules by completing the 1 question quiz"))
				minetest.show_formspec(p_name, "tos:tos_fs", make_formspec())
				return
			end

	elseif fields.no then
		minetest.kick_player(p_name, S("Please show you understand the rules."))
		return

	else
		minetest.show_formspec(p_name, "tos:tos_fs", make_formspec())
	end
end)





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function make_formspec()
	local word = S("never")
	local formspec = {
	  "size[9,8]",
	  "textarea[0.5,0.5;8,7;TOS;".. S("SERVER RULES") .. ";"..TOS.."]",
	  "field[0.5,7.5;6,1;entry;" .. S("When can I insult a user? One word, lowercase") .. ";]",
	  "button_exit[6,7.4;1.5,0.5;ok;" .. S("Play!") .. "]",
	  "button[7.5,7.4;1.5,0.5;no;" .. S("Goodbye!") .. "]",
	  "field[10,10;0.1,0.1;hidden_word;;"..word.."]",
	  "field_close_on_enter[;false]"
	}
	return table.concat(formspec, "")
end
